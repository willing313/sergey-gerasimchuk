package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner str = new Scanner(System.in);
        System.out.print("Введите слово, состоящее из четного числа букв: ");
        String str1 = str.next();
        String str2 = str1.substring(0,(str1.length()/2));
        if(str1.length()%2==0)
        System.out.println("первая половина слова "+str2);
        else System.out.println("слово состоит из нечетного числа букв");
    }
}
